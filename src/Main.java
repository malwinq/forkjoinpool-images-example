import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Nazwa pliku JPG: ");
        String fileName = reader.readLine();

        System.out.println("Threshold: ");
        int threshold = Integer.parseInt(reader.readLine());

        System.out.println("Rozmiar maski (liczba nieparzysta): ");
        int diffSize = Integer.parseInt(reader.readLine());

        ForkDiff.run(fileName, diffSize, threshold);
        Diff.run(fileName, diffSize);
    }
}
