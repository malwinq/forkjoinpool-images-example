import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.RecursiveAction;
import javax.imageio.ImageIO;

public class Diff {

    private final int[] source;
    private final int length;
    private final int[] destination;
    private final int diffSize;           // Rozmiar maski, liczba nieparzysta

    public Diff(int[] src, int length, int[] dst, int diffSize) {
        this.source = src;
        this.length = length;
        this.destination = dst;
        this.diffSize = diffSize;
    }

    // Obliczanie gradientu, metoda: maska Prewitta pozioma
    protected void computeDirectly() {
        int sidePixels = (int) (Math.pow(diffSize, 2) - 1) / 2;

        // Dla każdego piksela po kolei
        for (int index = 0; index < length; index++) {
            float gray = 0;

            // Obliczanie wartosci piksela wynikowego (przejście po pikselach sąsiadujących)
            for (int mi = -sidePixels; mi <= (-sidePixels) / 2; mi++) {
                int mindex = Math.min(Math.max(mi + index, 0), source.length - 1);
                int pixel = source[mindex];
                gray -= (rgbToGrayScale(pixel) / Math.pow(diffSize, 2));
            }
            for (int mi = sidePixels / 2; mi <= sidePixels; mi++) {
                int mindex = Math.min(Math.max(mi + index, 0), source.length - 1);
                int pixel = source[mindex];
                gray += (rgbToGrayScale(pixel) / Math.pow(diffSize, 2));
            }

            // Złożenie kolorów piksela wyjściowego
            int dpixel = (0xff000000)
                    | (((int) gray) << 16)
                    | (((int) gray) << 8)
                    | (((int) gray));
            destination[index] = dpixel;
        }
    }

    protected void compute() {
        // Oblicz bezposrednio - bez podzialu na podobrazy
        computeDirectly();
    }

    public static void run(String fileName, int diffSize) throws Exception {
        System.out.println("WITHOUT FORKING =================================================================================");

        // Wczytanie obrazu
        String srcName = "D:\\repo\\forkjoinpool-images-example\\resources\\".concat(fileName).concat(".jpg");
        File srcFile = new File(srcName);
        BufferedImage image = ImageIO.read(srcFile);

        // Obliczenie gradientu
        BufferedImage diffImage = differentiate(image, diffSize);

        // Zapisanie wyniku
        String dstName = "D:\\repo\\forkjoinpool-images-example\\resources\\differentiated_".concat(fileName).concat("_diff.png");
        File dstFile = new File(dstName);
        ImageIO.write(diffImage, "png", dstFile);

        System.out.println("Output image: " + dstName);

    }

    public static BufferedImage differentiate(BufferedImage srcImage, int diffSize) {
        int width = srcImage.getWidth();
        int height = srcImage.getHeight();

        int[] src = srcImage.getRGB(0, 0, width, height, null, 0, width);
        int[] dst = new int[src.length];

        Diff diff = new Diff(src, src.length, dst, diffSize);
        long startTime = System.currentTimeMillis();

        // Wywołanie metody
        diff.compute();
        long endTime = System.currentTimeMillis();

        System.out.println("Image differentiation took " + (endTime - startTime) + " milliseconds.");

        BufferedImage dstImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        dstImage.setRGB(0, 0, width, height, dst, 0, width);

        return dstImage;
    }

    // Obliczanie jasności w skali szarości
    private float rgbToGrayScale(int pixel) {
        float red = (float) (((pixel & 0x00ff0000) >> 16) * 0.299);
        float green = (float) (((pixel & 0x00ff0000) >> 16) * 0.587);
        float blue = (float) (((pixel & 0x00ff0000) >> 16) * 0.114);

        return red + green + blue;
    }
}